/**
 * Created by derrickow on 8/28/17.
 */
import java.util.Random;
import java.util.Scanner;

public class Connect_Four {

    private final static int NUM_COL = 7;
    private final static int NUM_ROW = 6;
    private char [][] board;
    private char player_piece;
    private char player1_piece;
    private char player2_piece;
    private int player_num;
    private int column_num;
    private boolean win;
    private boolean tie;
    private boolean second_check;
    private boolean third_check;


    // one-time setup of the board
    private void setup_board() {
        this.board = new char [NUM_ROW][NUM_COL];
        for (int i = 0; i < NUM_ROW; i ++) {
            for (int j = 0; j < NUM_COL; j++) {
                board[i][j] = '.';
            }
        }
    }

    // players choose pieces and turns are decided
    private void game_setup(){
        win = false;
        tie = false;
        second_check = false;
        third_check = false;

        Scanner scnr = new Scanner(System.in);
        System.out.println("Welcome to Connect Four!");
        System.out.println("Player 1, choose your piece!: ");
        while(true){
            player1_piece = scnr.next(".").charAt(0);
            if (player1_piece == 46) // check if player chose '.'
                System.out.println("Please select a different piece: ");
            else
                break;
        }

        System.out.println("Player 2, choose your piece!: ");

        while(true) {
            player2_piece = scnr.next(".").charAt(0);
            if (player2_piece == player1_piece) {
                System.out.println("Select a piece different from Player 1!");
            }
            else if (player2_piece == 46)
                System.out.println("Please select a different piece: ");
            else
                break;
        }

        this.first_turn();
    }

    // displays board through out game
    private void display_board(){
        for (int i = 0; i < NUM_ROW; i++) {
            for (int j = 0; j < NUM_COL; j++) {
                System.out.print(board[i][j]);
                System.out.print(" ");
            }

            System.out.println();
        }
        System.out.println();
        for (int k = 1; k < NUM_COL+1; k++){
            System.out.print(k + " ");
        }
        System.out.println("\n");
    }

    // random number generator decides who goes first
    private void first_turn(){
        Random rand = new Random();
        player_num = rand.nextInt(2) + 1; // 2 is the maximum and 1 is the minimum

        StringBuilder sb = new StringBuilder();
        sb.append(player_num);

        System.out.println("Player " + sb.toString() + " goes first! Select the column number to place your piece.");
    }

    // player chooses column
    private void player_turn(int player_num){

        Scanner piece = new Scanner(System.in);
        System.out.println("Player " + (player_num) + "'s turn: ");

        // check for valid column number entered
        boolean valid_move = false;
        boolean valid_Col = false;
        String string_col;
        while(!valid_move) {
            string_col = piece.nextLine();

            column_num = string_col.charAt(0)-'0'; //convert first character from String to char then to int

            // check for valid column number
            if ((column_num >= 1) && (column_num <= 7) && (string_col.length() == 1))
                valid_Col = true;
            else
                System.out.println(string_col +" is not a valid column number! Player " +player_num + ", please try again: ");

            // check if column is full
            if ((valid_Col) && (column_num >= 1) && (column_num <=7) && (string_col.length() == 1)) {
                if (board[0][column_num-1] == 46) // check if top of column is empty '.'
                    valid_move = true;
                else {
                    System.out.println("Column full! Player " + player_num + ", please choose another column: ");
                }
            }
        }

    }

    // piece is dropped
    private void place_piece(int player_num, int column_num){

        if (player_num == 1)
            player_piece = player1_piece;
        else
            player_piece = player2_piece;

        // check which row the piece will fall onto
        for (int i = NUM_ROW-1; i > -1; i--) {
            if (board[i][column_num-1] == '.'){
                board[i][column_num-1] = player_piece;
                break;
            }
        }

    }

    private void change_turn(){
        if (player_num == 1)
            player_num = 2;
        else
            player_num = 1;
    }

    private boolean check_horizontal(char player_piece){
        boolean horizontal_check = false;
        int counter = 0;
            for (int i = 0; i < NUM_ROW; i++) {
                counter = 0; // resets for each row
                for (int j = 0; j < NUM_COL; j++) {
                    if (board[i][j] == player_piece) {
                        counter++;
                    }
                    else {
                        counter = 0;
                    }
                    if (counter >= 4) {
                        horizontal_check = true;
                    }
                }
            }

        return horizontal_check;
    }

    private boolean check_vertical(char player_piece){
        boolean vertical_check = false;
        int counter = 0;

        for (int i = 0; i < NUM_COL; i++) {
            counter = 0; // resets for each column
            for (int j = 0; j < NUM_ROW; j++) {
                if (board[j][i] == player_piece) {
                    counter++;
                }
                else {
                    counter = 0;
                }
                if (counter >= 4) {
                    vertical_check = true;
                }
            }
        }

        return vertical_check;
    }

    private boolean check_left_diagonal(char player_piece){
        boolean left_diagonal_check = false;
        int counter = 0;

        for (int i = 3; i < NUM_ROW; i++){
            counter = 0;
            switch(i) {
                case 3:
                    int j = 0;
                    for (int k = 3; k > -1; k--) {
                        if (board[k][j++] == player_piece)
                            counter++;
                        else
                            counter = 0;
                        if (counter >= 4)
                            left_diagonal_check = true;
                    }
                    break;

                case 4:
                    j = 0;
                    for (int k = 4; k > -1; k--) {
                        if (board[k][j++] == player_piece)
                            counter++;
                        else
                            counter = 0;
                        if (counter >= 4)
                            left_diagonal_check = true;
                    }
                    break;

                case 5:
                    j = 0;
                    counter = 0;
                    for (int k = 5; k > -1; k--) {
                        if (board[k][j++] == player_piece)
                            counter++;
                        else
                            counter = 0;
                        if (counter >= 4)
                            left_diagonal_check = true;
                    }

                    if (left_diagonal_check == false){
                        j = 1;
                        counter = 0;
                        second_check = true;
                        for (int k = 5; k > -1; k--) {
                            if (board[k][j++] == player_piece)
                                counter++;
                            else
                                counter = 0;
                            if (counter >= 4)
                                left_diagonal_check = true;
                        }
                    }

                    if (second_check && !left_diagonal_check && j <= NUM_ROW+1){
                        j = 2;
                        counter = 0;
                        third_check = true;
                        for (int k = 5; k > 0; k--) {
                            if (board[k][j++] == player_piece)
                                counter++;
                            else
                                counter = 0;
                            if (counter >= 4)
                                left_diagonal_check = true;
                        }
                    }

                    if (third_check && !left_diagonal_check && j <= NUM_ROW+1){
                        j = 3;
                        counter = 0;
                        for (int k = 5; k > 1; k--) {
                            if (board[k][j++] == player_piece)
                                counter++;
                            else
                                counter = 0;
                            if (counter >= 4)
                                left_diagonal_check = true;
                        }
                    }

                    break;

                default:
                    break;
            }

        }

        // if there isn't a win, reset the checks to false
        if (!left_diagonal_check){
            second_check = false;
            third_check = false;
        }

        return left_diagonal_check;
    }

    private boolean check_right_diagonal(char player_piece){
        boolean right_diagonal_check = false;
        int counter = 0;

        for (int i = 0; i < 3; i++){
            counter = 0;
            switch(i) {
                case 2:
                    int j = 0;
                    for (int k = 2; k < NUM_ROW; k++) {
                        if (board[k][j++] == player_piece)
                            counter++;
                        else
                            counter = 0;
                        if (counter >= 4)
                            right_diagonal_check = true;
                    }
                    break;

                case 1:
                    j = 0;
                    for (int k = 1; k < NUM_ROW; k++) {
                        if (board[k][j++] == player_piece)
                            counter++;
                        else
                            counter = 0;
                        if (counter >= 4)
                            right_diagonal_check = true;
                    }
                    break;

                case 0:
                    j = 0;
                    counter = 0;
                    for (int k = 0; k < NUM_ROW; k++) {
                        if (board[k][j++] == player_piece)
                            counter++;
                        else
                            counter = 0;
                        if (counter >= 4)
                            right_diagonal_check = true;
                    }

                    if (right_diagonal_check == false){
                        j = 1;
                        counter = 0;
                        second_check = true;
                        for (int k = 0; k < NUM_ROW; k++) {
                            if (board[k][j++] == player_piece)
                                counter++;
                            else
                                counter = 0;
                            if (counter >= 4)
                                right_diagonal_check = true;
                        }
                    }

                    if (second_check && !right_diagonal_check && j <= NUM_ROW+1){
                        j = 2;
                        counter = 0;
                        third_check = true;
                        for (int k = 0; k < NUM_ROW-1; k++) {
                            if (board[k][j++] == player_piece)
                                counter++;
                            else
                                counter = 0;
                            if (counter >= 4)
                                right_diagonal_check = true;
                        }
                    }

                    if (third_check && !right_diagonal_check && j <= NUM_ROW+1){
                        j = 3;
                        counter = 0;
                        for (int k = 0; k < NUM_ROW-2; k++) {
                            if (board[k][j++] == player_piece)
                                counter++;
                            else
                                counter = 0;
                            if (counter >= 4)
                                right_diagonal_check = true;
                        }
                    }

                    break;

                default:
                    break;
            }

        }

        // if there isn't a win, reset the checks to false
        if (!right_diagonal_check){
            second_check = false;
            third_check = false;
        }

        return right_diagonal_check;
    }


    private boolean check_victory(int player_num){
        win = check_horizontal(player_piece) || check_vertical(player_piece) || check_left_diagonal(player_piece) || check_right_diagonal(player_piece);
        if (win)
            System.out.println("Player " + (player_num) + " wins!");

        return win;
    }

    private boolean check_tie(){
        // check the top row
        for (int j = 0; j < NUM_COL; j++) {
            if (board[0][j] == '.') {
                tie = false;
                break;
            }
            else
                tie = true;
        }

        if (tie)
            System.out.println("Tie Game!");

        return tie;
    }

    private void run() {
        while(!win) {
            this.player_turn(player_num);
            this.place_piece(player_num,column_num);
            this.display_board();
            if (this.check_victory(player_num) || this.check_tie())
                break;
            else
                this.change_turn();
        }
    }

    private void setup() {
        this.setup_board();
        this.game_setup();
        this.display_board();
    }

    public static void main (String [] args){
        Connect_Four connect_four = new Connect_Four();
        boolean cont = true;
        while(cont){
            connect_four.setup();
            connect_four.run();

            Scanner scnr = new Scanner(System.in);
            System.out.println();
            System.out.println("Do you want to play again? (y/n)");
            char response;
            while(true){
                response = scnr.next(".").charAt(0);
                if (response != 'y'){
                    // check if player chose '.'
                    cont = false;
                    System.out.println();
                }
                break;
            }
        }

    }

}
